# Magic Code E-Mail Login

This submodule implements a magic code based login for drupal besides
the built-in username / password login.

It works similar to the Magic-Code Login in e.g. Slack.

## Forms

Provides two new forms:

- `MagicCodeEmailLoginForm` (route: `/user/login/magic-code/email`)
  Form where the user enters the email address to receive a magic code for
  login.
- `MagicCodeEmailLoginVerifyForm` (route: `/user/login/magic-code/email/verify`)
  Form where the user enters the magic code to finalize the login.
  The form supports resending of the magic code.

## Mails

Implements a new mail that is sent to the user whenever the login form is
submitted.

The email can be configured in the account settings
(`/admin/config/people/accounts`) under `Emails > Magic Code Login`.

Use the `[user:magic-code-login]` token to generate a magic code for login.  
See [main module readme](../../README.md) for more information about token usage.

The email can be sent using the `_magic_code_email_login_mail_notify` function.

## Flood

The forms integrate with the magic code and drupal core's flood control.

When submitting the `MagicCodeEmailLoginForm` form the following flood
events are checked and must not be triggered:

- `user.failed_login_user`
- `user.failed_login_ip`
- `magic_code.creation_user`
- `magic_code.creation_ip`

If any of those events are triggered an appropriate error message is displayed
on form submit.

---

When verifying the code via the `MagicCodeEmailLoginVerifyForm` form the
following flood events are explicitly checked:

- `user.failed_login_user`
- `user.failed_login_ip`

Additionally the default flood integration of the main `magic_code` module is
also checked on verification and an appropriate error message is displayed on
form submit if the magic code verification flood events are triggered.

**When using the resend code functionality on this form, the same flood events
as of `MagicCodeEmailLoginForm` are checked!**
