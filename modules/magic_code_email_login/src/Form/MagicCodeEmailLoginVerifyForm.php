<?php

declare(strict_types=1);

namespace Drupal\magic_code_email_login\Form;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\magic_code\Flood\MagicCodeFloodControlInterface;
use Drupal\magic_code\MagicCodeManagerInterface;
use Drupal\magic_code\MagicCodeResult;
use Drupal\magic_code_email_login\MagicCodeEmailLoginFloodError;
use Drupal\magic_code_email_login\MagicCodeEmailLoginFloodTrait;
use Drupal\magic_code_verify_form\Form\MagicCodeVerifyFormBase;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form to verify magic code email login.
 */
class MagicCodeEmailLoginVerifyForm extends MagicCodeVerifyFormBase {

  use AutowireTrait;
  use MagicCodeEmailLoginFloodTrait;

  /**
   * The user entity storage.
   */
  protected EntityStorageInterface $userStorage;

  /**
   * Construct new instance.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected FloodInterface $floodControl,
    protected MagicCodeFloodControlInterface $magicCodeFlood,
    protected BareHtmlPageRendererInterface $bareHtmlPageRenderer,
    protected MagicCodeManagerInterface $magicCodeManager,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'magic_code_email_login_verification_form';
  }

  /**
   * {@inheritdoc}
   */
  public function bareHtmlPageRenderer(): BareHtmlPageRendererInterface {
    return $this->bareHtmlPageRenderer;
  }

  /**
   * {@inheritdoc}
   */
  public function floodControl(): FloodInterface {
    return $this->floodControl;
  }

  /**
   * {@inheritdoc}
   */
  public function magicCodeFlood(): MagicCodeFloodControlInterface {
    return $this->magicCodeFlood;
  }

  /**
   * {@inheritdoc}
   */
  public function userStorage(): EntityStorageInterface {
    return $this->userStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $email = $this->getRequest()->get('email');
    if (!$email) {
      $this->messenger()->addError(
        $this->t('E-Mail not set, please try again!')
      );

      $destination = $this->getRequest()->get('destination');
      $this->getRequest()->query->remove('destination');

      $url = Url::fromRoute('magic_code.email_login', [
        'destination' => $destination,
      ]);

      return new RedirectResponse($url->toString());
    }

    $form = parent::buildForm($form, $form_state);

    $form['info']['#value'] = $this->t('Please enter the 6-digit code that was sent to <em>@email</em>:', [
      '@email' => $email,
    ]);

    $form['#cache']['contexts'][] = 'url.query_args';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function verify(string $code, FormStateInterface $form_state): void {
    $email = $this->getRequest()->get('email');

    // Check if user is blocked by drupal core flood.
    if ($result = $this->isUserCoreFloodBlocked($email)) {
      switch ($result) {
        case MagicCodeEmailLoginFloodError::IPBlocked:
          $this->setFloodBlockResponse(TRUE, $form_state);
          return;

        case MagicCodeEmailLoginFloodError::UserBlocked:
          $this->setFloodBlockResponse(FALSE, $form_state);
          return;
      }
    }

    $userResult = $this->userStorage->loadByProperties([
      'mail' => $email,
    ]);

    /** @var \Drupal\user\UserInterface $user */
    $user = reset($userResult);
    if (!$user) {
      $form_state->setErrorByName($this->t('No user found for email @email', [
        '@email' => $email,
      ]));
    }

    $result = $this->magicCodeManager->verify(
      $code,
      'login',
      MagicCodeManagerInterface::VERIFY_MODE_LOGIN,
      $user,
    );

    switch ($result) {
      case MagicCodeResult::Success:
        $this->login($user, $form_state);
        return;

      case MagicCodeResult::Invalid:
        $this->messenger()->addError($this->t('The code is invalid or has already been used.'));
        $form_state->disableRedirect();
        return;

      case MagicCodeResult::BlockedByIp:
        $this->setFloodBlockResponse(TRUE, $form_state);
        return;

      case MagicCodeResult::BlockedByUser:
        $this->setFloodBlockResponse(FALSE, $form_state);
        return;

      default:
        $this->messenger()->addError($this->t('The code could not be verified!'));
        $form_state->disableRedirect();
        return;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function resend(FormStateInterface $form_state): void {
    $email = $this->getRequest()->get('email');
    if (!$email) {
      $this->messenger()->addError($this->t('Could not get email to resend code to.'));
      return;
    }

    $result = $this->canCreateMagicCodeForEmail($email);

    // If $result is a user, resend the email.
    if ($result instanceof UserInterface) {
      _magic_code_email_login_mail_notify('login', $result);

      $this->messenger()->addStatus($this->t('A new code has been sent to @email!', [
        '@email' => $email,
      ]));

      $destination = $this->getRequest()->get('destination');
      $this->getRequest()->query->remove('destination');

      $form_state->setRedirect('magic_code.email_login_verify', [
        'destination' => $destination,
        'email' => $email,
      ]);
      return;
    }

    // Handle errors.
    switch ($result) {
      case $result::UnrecognizedEmail:
        $form_state->setErrorByName('email', $this->t('Unrecognized e-mail address.'));
        break;

      case $result::AccountInactive:
        $form_state->setErrorByName('email', $this->t('The account %name has not been activated or is blocked.', ['%name' => $form_state->getValue('email')]));
        break;

      case $result::UserBlocked:
        $this->setFloodBlockResponse(FALSE, $form_state);
        break;

      case $result::IPBlocked:
        $this->setFloodBlockResponse(TRUE, $form_state);
        break;

      default:
        $form_state->setErrorByName('email', $this->t('Unknown error occured.'));
        break;
    }
  }

  /**
   * Log in the user.
   */
  private function login(AccountInterface $account, FormStateInterface $form_state) {
    $this->removeEmailFromSession();

    // A destination was set, probably on an exception controller.
    if (!$this->getRequest()->request->has('destination')) {
      $form_state->setRedirect(
        'entity.user.canonical',
        ['user' => $account->id()]
      );
    }
    else {
      $this->getRequest()->query->set('destination', $this->getRequest()->request->get('destination'));
    }

    user_login_finalize($account);
  }

}
