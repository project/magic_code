<?php

declare(strict_types=1);

namespace Drupal\magic_code_email_login\Form;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\Core\Url;
use Drupal\magic_code\Flood\MagicCodeFloodControlInterface;
use Drupal\magic_code_email_login\MagicCodeEmailLoginFloodTrait;
use Drupal\user\UserInterface;

/**
 * Magic Code E-Mail Login form.
 */
class MagicCodeEmailLoginForm extends FormBase {

  use AutowireTrait;
  use MagicCodeEmailLoginFloodTrait;

  /**
   * The user entity storage.
   */
  protected EntityStorageInterface $userStorage;

  /**
   * Construct new instance.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected FloodInterface $floodControl,
    protected MagicCodeFloodControlInterface $magicCodeFlood,
    protected BareHtmlPageRendererInterface $bareHtmlPageRenderer,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'magic_code_email_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function bareHtmlPageRenderer(): BareHtmlPageRendererInterface {
    return $this->bareHtmlPageRenderer;
  }

  /**
   * {@inheritdoc}
   */
  public function floodControl(): FloodInterface {
    return $this->floodControl;
  }

  /**
   * {@inheritdoc}
   */
  public function magicCodeFlood(): MagicCodeFloodControlInterface {
    return $this->magicCodeFlood;
  }

  /**
   * {@inheritdoc}
   */
  public function userStorage(): EntityStorageInterface {
    return $this->userStorage;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $destination = $this->getRequest()->get('destination');

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-Mail'),
      '#size' => 60,
      '#required' => TRUE,
      '#attributes' => [
        'autocorrect' => 'none',
        'autocapitalize' => 'none',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
        'autocomplete' => 'email',
        'placeholder' => $this->t('email@example.com'),
      ],
    ];

    $form['login_hint'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'form-description',
      ],
      '#value' => $this->t('You will receive a <strong>code</strong> via email with which you can log in.'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send code'),
      '#button_type' => 'primary',
    ];

    $form['actions']['login_password'] = [
      '#type' => 'html_tag',
      '#tag' => 'a',
      '#attributes' => [
        'class' => 'button button--secondary button--login-with-pass',
        'href' => Url::fromRoute('user.login', ['destination' => $destination])->toString(),
      ],
      '#value' => $this->t('Login with password'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If a response has been set, do nothing.
    if ($form_state->getResponse() !== NULL) {
      return;
    }

    // Send mail.
    _magic_code_email_login_mail_notify('login', $form_state->get('user'));

    $destination = $this->getRequest()->get('destination');
    $this->getRequest()->query->remove('destination');

    $params = [
      'email' => $form_state->getValue('email'),
    ];

    if ($destination) {
      $params['destination'] = $destination;
    }

    // Redirect to verification form.
    $form_state->setRedirect('magic_code.email_login_verify', $params);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $result = $this->canCreateMagicCodeForEmail($form_state->getValue('email') ?? '');

    // If $result is a user, everything was successful!
    if ($result instanceof UserInterface) {
      $form_state->set('user', $result);
      return;
    }

    switch ($result) {
      case $result::UnrecognizedEmail:
        $form_state->setErrorByName('email', $this->t('Unrecognized e-mail address.'));
        break;

      case $result::AccountInactive:
        $form_state->setErrorByName('email', $this->t('The account %name has not been activated or is blocked.', ['%name' => $form_state->getValue('email')]));
        break;

      case $result::UserBlocked:
        $this->setFloodBlockResponse(FALSE, $form_state);
        break;

      case $result::IPBlocked:
        $this->setFloodBlockResponse(TRUE, $form_state);
        break;

      default:
        $form_state->setErrorByName('email', $this->t('Unknown error occured.'));
        break;
    }
  }

}
