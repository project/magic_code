<?php

declare(strict_types=1);

namespace Drupal\magic_code_email_login;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\BareHtmlPageRendererInterface;
use Drupal\magic_code\Flood\MagicCodeFloodControlInterface;
use Drupal\magic_code\Flood\MagicCodeFloodOperation;
use Drupal\user\UserInterface;

/**
 * Trait that implements email login flood functionality.
 */
trait MagicCodeEmailLoginFloodTrait {

  /**
   * Get the bareHtmlPageRenderer.
   */
  abstract public function bareHtmlPageRenderer(): BareHtmlPageRendererInterface;

  /**
   * Get the flood control service.
   */
  abstract public function floodControl(): FloodInterface;

  /**
   * Get the magic code flood control.
   */
  abstract public function magicCodeFlood(): MagicCodeFloodControlInterface;

  /**
   * Get the user storage.
   */
  abstract public function userStorage(): EntityStorageInterface;

  /**
   * Get the current request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The request.
   */
  abstract public function getRequest();

  /**
   * Retrieves a configuration object.
   *
   * This is the main entry point to the configuration API. Calling.
   * @code $this->config('book.admin') @endcode will return a configuration
   * object in which the book module can store its administrative settings.
   *
   * @param string $name
   *   The name of the configuration object to retrieve. The name corresponds to
   *   a configuration file. For @code \Drupal::config('book.admin') @endcode,
   *   the config object returned will contain the contents of book.admin
   *   configuration file.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   A configuration object.
   */
  abstract public function config($name);

  /**
   * Checks if user is blocked by the core user flood.
   */
  protected function isUserCoreFloodBlocked(string $email) {
    $floodConfig = $this->config('user.flood');

    // Load user.
    $userResult = $this->userStorage()->loadByProperties([
      'mail' => $email,
    ]);
    /** @var \Drupal\user\UserInterface $user */
    $user = reset($userResult);

    if (!$user) {
      return MagicCodeEmailLoginFloodError::UnrecognizedEmail;
    }

    $ipAllowed = $this->floodControl()->isAllowed('user.failed_login_ip', $floodConfig->get('ip_limit'), $floodConfig->get('ip_window'));
    if (!$ipAllowed) {
      return MagicCodeEmailLoginFloodError::IPBlocked;
    }

    $userAllowed = $this->floodControl()->isAllowed('user.failed_login_user', $floodConfig->get('user_limit'), $floodConfig->get('user_window'), $this->getUserFloodIdentifier($user));
    if (!$userAllowed) {
      return MagicCodeEmailLoginFloodError::UserBlocked;
    }

    return NULL;
  }

  /**
   * Check if a magic code can be generated for given email.
   */
  protected function canCreateMagicCodeForEmail(string $email) {
    // Check first if user is blocked by core flood.
    if ($result = $this->isUserCoreFloodBlocked($email)) {
      return $result;
    }

    // Load user.
    $userResult = $this->userStorage()->loadByProperties([
      'mail' => $email,
    ]);
    /** @var \Drupal\user\UserInterface $user */
    $user = reset($userResult);

    // Check if user exists for email address.
    if (!$user) {
      return MagicCodeEmailLoginFloodError::UnrecognizedEmail;
    }
    else {
      $userAllowed = $this->magicCodeFlood()->isUserAllowed(MagicCodeFloodOperation::Creation, $user->id());

      // Check if blocked.
      if ($user->isBlocked()) {
        return MagicCodeEmailLoginFloodError::AccountInactive;
      }

      // Check if user is blocked.
      if (!$userAllowed) {
        return MagicCodeEmailLoginFloodError::UserBlocked;
      }
    }

    // Check if IP is blocked.
    if (!$this->magicCodeFlood()->isIpAllowed(MagicCodeFloodOperation::Creation)) {
      return MagicCodeEmailLoginFloodError::IPBlocked;
    }

    return $user;
  }

  /**
   * Display the flood block info to user.
   */
  protected function setFloodBlockResponse(bool $isIp, FormStateInterface $form_state) {
    if ($isIp) {
      $message = $this->t('Too many failed code inputs or too many codes requested from your IP address. This IP address is temporarily blocked. Please try again later.');
    }
    else {
      $message = $this->t('Too many failed code inputs or too many codes requested. You have been temporarily blocked from further inputs. Please try again later.');
    }

    $response = $this->bareHtmlPageRenderer()->renderBarePage(['#markup' => $message], 'Verification failed', 'maintenance_page');
    $response->setStatusCode(403);

    // We need to clear all errors for the response to work.
    $form_state->clearErrors();

    $form_state->setResponse($response);
  }

  /**
   * Save the email on the current session.
   */
  protected function saveEmailOnSession(string $email) {
    $session = $this->getRequest()->getSession();
    $session->set('magic_code_email_login.verify_email', $email);
  }

  /**
   * Get the email from the current session.
   */
  protected function getEmailFromSession() {
    $session = $this->getRequest()->getSession();

    return $session->get('magic_code_email_login.verify_email');
  }

  /**
   * Remove email from session.
   */
  protected function removeEmailFromSession() {
    $session = $this->getRequest()->getSession();
    $session->remove('magic_code_email_login.verify_email');
  }

  /**
   * Get the flood user identifier.
   */
  private function getUserFloodIdentifier(UserInterface $user): string {
    $floodConfig = $this->config('user.flood');

    if ($floodConfig->get('uid_only')) {
      // Register flood events based on the uid only, so they apply for any
      // IP address. This is the most secure option.
      $identifier = $user->id();
    }
    else {
      // The default identifier is a combination of uid and IP address. This
      // is less secure but more resistant to denial-of-service attacks that
      // could lock out all users with public user names.
      $identifier = $user->id() . '-' . $this->getRequest()->getClientIP();
    }

    return $identifier;
  }

}
