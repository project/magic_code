<?php

declare(strict_types=1);

namespace Drupal\magic_code_email_login;

/**
 * Defines magic code email login flood errors.
 */
enum MagicCodeEmailLoginFloodError: string {
  case UnrecognizedEmail = 'unrecognized_email';
  case AccountInactive = 'account_inactive';
  case IPBlocked = 'ip_blocked';
  case UserBlocked = 'user_blocked';
}
