<?php

declare(strict_types=1);

namespace Drupal\Tests\magic_code_email_login\Functional;

use Drupal\Core\Test\AssertMailTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\magic_code\MagicCodeManagerInterface;

/**
 * Test the magic code login form.
 */
class MagicCodeLoginFormTest extends BrowserTestBase {

  use AssertMailTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'magic_code',
    'magic_code_email_login',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Magic code manager.
   */
  protected MagicCodeManagerInterface $magicCodeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->magicCodeManager = $this->container->get(MagicCodeManagerInterface::class);
  }

  /**
   * Test login via magic code.
   *
   * Tests submitting the send code form
   * and the verify form.
   */
  public function testMagicCodeLogin() {
    $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    $this->drupalGet('user/login/magic-code/email');
    $this->assertSession()->responseContains('action="/user/login/magic-code/email"');

    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    // Make sure we got redirected to the verify route.
    $this->assertStringContainsString('/user/login/magic-code/email/verify', $this->getUrl());

    // Make sure the magic code email was sent.
    $this->assertMail('id', 'magic_code_email_login_login');

    // Extract code from email body.
    $body = $this->getMails()[0]['body'];
    $matches = [];
    preg_match('/[0-9A-Z]{3}-[0-9A-Z]{3}/', $body, $matches);
    $this->assertTrue(isset($matches[0]));
    $code = $matches[0];

    $plainCode = str_replace('-', '', $code);
    $codeChars = str_split($plainCode);

    $this->submitForm([
      'code_input_1' => $codeChars[0],
      'code_input_2' => $codeChars[1],
      'code_input_3' => $codeChars[2],
      'code_input_4' => $codeChars[3],
      'code_input_5' => $codeChars[4],
      'code_input_6' => $codeChars[5],
    ], 'Verify');

    // Check if user is logged in.
    $this->assertSession()->responseContains('Member for');
  }

  /**
   * Test login via magic code.
   *
   * Tests submitting the send code form
   * and the verify form.
   */
  public function testMagicCodeLoginUnrecognizedEmail() {
    $this->drupalGet('user/login/magic-code/email');
    $this->assertSession()->responseContains('action="/user/login/magic-code/email"');

    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    // Error message must be present.
    $this->assertSession()->responseContains('Unrecognized e-mail address.');
  }

  /**
   * Test the magic code verification IP flood protection.
   */
  public function testMagicCodeVerificationFloodControl() {
    $this->config('magic_code.settings')
      ->set('flood_verification', [
        'ip_limit' => 10,
        'ip_window' => 3600,
        // Set a high per-user limit out so that it is not relevant in the test.
        'user_limit' => 4000,
        'user_window' => 21600,
      ])
      ->save();

    $user1 = $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Directly go to the verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // Try 2 failed logins.
    for ($i = 0; $i < 2; $i++) {
      $this->assertFailedMagicCodeLogin('000-00' . $i);
    }

    // A successful login will not reset the IP-based flood control count.
    $this->drupalLogin($user1);
    $this->drupalLogout();

    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // Try 8 more failed logins, they should not trigger the flood control
    // mechanism.
    for ($i = 0; $i < 8; $i++) {
      $this->assertFailedMagicCodeLogin('000-00' . $i);
    }

    // The next login trial should result in an IP-based flood error message.
    $this->assertFailedMagicCodeLogin('000-000', 'ip');

    // Got to verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // A login with the correct code should also result in a flood error
    // message.
    $validCode = $this->magicCodeManager->createNew('login', $user1, NULL, 'test@example.com');
    $this->assertFailedMagicCodeLogin($validCode->label(), 'ip');
  }

  /**
   * Test the magic code creation IP flood protection.
   */
  public function testMagicCodeCreationIpFloodControl() {
    $this->config('magic_code.settings')
      ->set('flood_creation', [
        'ip_limit' => 3,
        'ip_window' => 3600,
        // Set a high per-user limit out so that it is not relevant in the test.
        'user_limit' => 4000,
        'user_window' => 21600,
      ])
      ->save();

    $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Try 3 failed logins.
    for ($i = 0; $i < 3; $i++) {
      // Go to the login with magic code page.
      $this->drupalGet('user/login/magic-code/email');

      // Submit form, which creates a magic code.
      $this->submitForm([
        'email' => 'test@example.com',
      ], 'Send code');
    }

    // Go to the login with magic code page.
    $this->drupalGet('user/login/magic-code/email');

    // Submit form, which creates a magic code.
    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    $this->assertSession()->pageTextContains('Too many failed code inputs or too many codes requested from your IP address. This IP address is temporarily blocked. Please try again later.');
  }

  /**
   * Test the magic code creation user flood protection.
   */
  public function testMagicCodeCreationUserFloodControl() {
    $this->config('magic_code.settings')
      ->set('flood_creation', [
        'ip_limit' => 4000,
        'ip_window' => 3600,
        'user_limit' => 3,
        'user_window' => 900,
      ])
      ->save();

    $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Try 3 failed logins.
    for ($i = 0; $i < 3; $i++) {
      // Go to the login with magic code page.
      $this->drupalGet('user/login/magic-code/email');

      // Submit form, which creates a magic code.
      $this->submitForm([
        'email' => 'test@example.com',
      ], 'Send code');
    }

    // Go to the login with magic code page.
    $this->drupalGet('user/login/magic-code/email');

    // Submit form, which creates a magic code.
    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    $this->assertSession()->pageTextContains('Too many failed code inputs or too many codes requested. You have been temporarily blocked from further inputs. Please try again later.');
  }

  /**
   * Test the magic code creation IP flood protection at resend.
   */
  public function testMagicCodeCreationIpFloodControlResend() {
    $this->config('magic_code.settings')
      ->set('flood_creation', [
        'ip_limit' => 3,
        'ip_window' => 3600,
        // Set a high per-user limit out so that it is not relevant in the test.
        'user_limit' => 4000,
        'user_window' => 21600,
      ])
      ->save();

    $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Go to the login with magic code page.
    $this->drupalGet('user/login/magic-code/email');
    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    // On the verify page.
    $this->submitForm([], 'Request new code');
    $this->submitForm([], 'Request new code');

    // 4th is blocked.
    $this->submitForm([], 'Request new code');

    $this->assertSession()->pageTextContains('Too many failed code inputs or too many codes requested from your IP address. This IP address is temporarily blocked. Please try again later.');
  }

  /**
   * Test the magic code creation user flood protection.
   */
  public function testMagicCodeCreationUserFloodControlResend() {
    $this->config('magic_code.settings')
      ->set('flood_creation', [
        'ip_limit' => 4000,
        'ip_window' => 3600,
        'user_limit' => 3,
        'user_window' => 900,
      ])
      ->save();

    $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Go to the login with magic code page.
    $this->drupalGet('user/login/magic-code/email');
    $this->submitForm([
      'email' => 'test@example.com',
    ], 'Send code');

    // On the verify page.
    $this->submitForm([], 'Request new code');
    $this->submitForm([], 'Request new code');

    // 4th is blocked.
    $this->submitForm([], 'Request new code');

    $this->assertSession()->pageTextContains('Too many failed code inputs or too many codes requested. You have been temporarily blocked from further inputs. Please try again later.');
  }

  /**
   * Test the global IP flood protection from the user module.
   */
  public function testMagicCodeCoreFloodControlIpBlocked() {
    $this->config('user.flood')
      ->set('ip_limit', 1)
      // Set a high per-user limit out so that it is not relevant in the test.
      ->set('user_limit', 4000)
      ->save();

    $user1 = $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Failed login.
    $edit = [
      'name' => $user1->getAccountName(),
      'pass' => 'wrong',
    ];
    for ($i = 0; $i < 2; $i++) {
      $this->drupalGet('user/login');
      $this->submitForm($edit, 'Log in');
    }

    // Directly go to the verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // A login with the correct code should also result in a flood error
    // message.
    $validCode = $this->magicCodeManager->createNew('login', $user1, NULL, 'test@example.com');
    $this->assertFailedMagicCodeLogin($validCode->label(), 'ip');
  }

  /**
   * Test the user flood protection.
   */
  public function testMagicCodeVerificationUserFloodControl() {
    $user1 = $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Directly go to the verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // Try 2 failed logins.
    for ($i = 0; $i < 2; $i++) {
      $this->assertFailedMagicCodeLogin('000-00' . $i);
    }

    // A successful login will reset the user based block.
    $validCode = $this->magicCodeManager->createNew('login', $user1, NULL, 'test@example.com');
    $this->assertSuccessfulMagicCodeLogin($validCode->label());

    // Logout.
    $this->drupalLogout();

    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // Try 5 more failed logins.
    for ($i = 0; $i < 5; $i++) {
      $this->assertFailedMagicCodeLogin('000-00' . $i);
    }

    // The next login trial should result in an IP-based flood error message.
    $this->assertFailedMagicCodeLogin('000-000', 'user');

    // Got to verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // A login with the correct code should also result in a flood error
    // message.
    $validCode = $this->magicCodeManager->createNew('login', $user1, NULL, 'test@example.com');
    $this->assertFailedMagicCodeLogin($validCode->label(), 'user');
  }

  /**
   * Test the user flood protection from the user module.
   */
  public function testMagicCodeCoreUserFloodControlUserBlocked() {
    $this->config('user.flood')
      ->set('ip_limit', 100)
      // Set a high per-user limit out so that it is not relevant in the test.
      ->set('user_limit', 1)
      ->save();

    $user1 = $this->drupalCreateUser(values: [
      'mail' => 'test@example.com',
    ]);

    // Failed login.
    $edit = [
      'name' => $user1->getAccountName(),
      'pass' => 'wrong',
    ];
    for ($i = 0; $i < 2; $i++) {
      $this->drupalGet('user/login');
      $this->submitForm($edit, 'Log in');
    }

    // Directly go to the verify page.
    $this->drupalGet('user/login/magic-code/email/verify', [
      'query' => [
        'email' => 'test@example.com',
      ],
    ]);

    // A login with the correct code should also result in a flood error
    // message.
    $validCode = $this->magicCodeManager->createNew('login', $user1, NULL, 'test@example.com');
    $this->assertFailedMagicCodeLogin($validCode->label(), 'user');
  }

  /**
   * Assert a failed login with given code.
   */
  protected function assertFailedMagicCodeLogin(string $code, ?string $floodTrigger = NULL) {
    $plainCode = str_replace('-', '', $code);
    $codeChars = str_split($plainCode);

    $this->submitForm([
      'code_input_1' => $codeChars[0],
      'code_input_2' => $codeChars[1],
      'code_input_3' => $codeChars[2],
      'code_input_4' => $codeChars[3],
      'code_input_5' => $codeChars[4],
      'code_input_6' => $codeChars[5],
    ], 'Verify');

    $assertSession = $this->assertSession();

    if (isset($floodTrigger)) {
      $assertSession->statusCodeEquals(403);

      if ($floodTrigger === 'ip') {
        $assertSession->pageTextContains('Too many failed code inputs or too many codes requested from your IP address. This IP address is temporarily blocked. Please try again later.');
      }
      else {
        $assertSession->pageTextContains('Too many failed code inputs or too many codes requested. You have been temporarily blocked from further inputs. Please try again later.');
      }
    }
    else {
      $assertSession->pageTextContains('The code is invalid or has already been used.');
    }
  }

  /**
   * Assert a successful login with given code.
   */
  protected function assertSuccessfulMagicCodeLogin(string $code) {
    $plainCode = str_replace('-', '', $code);
    $codeChars = str_split($plainCode);

    $this->submitForm([
      'code_input_1' => $codeChars[0],
      'code_input_2' => $codeChars[1],
      'code_input_3' => $codeChars[2],
      'code_input_4' => $codeChars[3],
      'code_input_5' => $codeChars[4],
      'code_input_6' => $codeChars[5],
    ], 'Verify');

    // Check if user is logged in.
    $this->assertSession()->pageTextContains('Member for');
  }

}
