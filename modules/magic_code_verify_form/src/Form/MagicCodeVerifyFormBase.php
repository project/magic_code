<?php

declare(strict_types=1);

namespace Drupal\magic_code_verify_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the base for magic code verification forms.
 */
abstract class MagicCodeVerifyFormBase extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $baseInput = [
      '#type' => 'textfield',
      '#size' => 1,
      '#required' => TRUE,
      '#attributes' => [
        'autocorrect' => 'none',
        'autocapitalize' => 'none',
        'spellcheck' => 'false',
        'autofocus' => 'autofocus',
        'autocomplete' => 'none',
        'maxlength' => 1,
      ],
      '#pattern' => '[a-zA-Z0-9]',
    ];

    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('Please enter the 6-digit code that was sent to you:'),
    ];

    $form['code_input'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'verify-code-input',
      ],
    ];

    $form['code_input']['container'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'verify-code-input__container',
      ],
    ];

    $form['code_input']['container']['code_input_1'] = $baseInput;
    $form['code_input']['container']['code_input_2'] = $baseInput;
    $form['code_input']['container']['code_input_3'] = $baseInput;

    $form['code_input']['container']['spacer'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => 'verify-code-input__spacer',
      ],
      '#value' => '—',
    ];

    $form['code_input']['container']['code_input_4'] = $baseInput;
    $form['code_input']['container']['code_input_5'] = $baseInput;
    $form['code_input']['container']['code_input_6'] = $baseInput;

    $form['code_input_error_container'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'class' => ['verify-code-input__error-container'],
      ],
    ];

    $form['#attached']['library'][] = 'magic_code_verify_form/form.verify';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Verify'),
      '#button_type' => 'primary',
    ];

    $form['actions']['resend'] = [
      '#id' => 'resend',
      '#type' => 'submit',
      '#value' => $this->t('Request new code'),
      '#button_type' => 'secondary',
      '#limit_validation_errors' => [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#id'] === "resend") {
      $this->resend($form_state);
      return;
    }

    $code = $this->getCode($form_state);
    $this->verify($code, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    if ($trigger['#id'] === "resend") {
      $form_state->clearErrors();
      return;
    }

    $code = $this->getCode($form_state);

    // Validate code.
    if (!preg_match('/^[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}$/m', $code)) {
      $form_state->setErrorByName('code_input_error_container', $this->t('Invalid code format. Please enter only digits (0-9) and characters (A-Z).'));
    }
  }

  /**
   * Verifies the magic code.
   */
  abstract public function verify(string $code, FormStateInterface $form_state): void;

  /**
   * Send the magic code to the user again.
   */
  abstract public function resend(FormStateInterface $form_state): void;

  /**
   * Gets the verification code from form state.
   */
  protected function getCode(FormStateInterface $form_state) {
    $code = '';

    for ($i = 1; $i <= 6; $i++) {
      $code .= $form_state->getValue('code_input_' . $i);

      if ($i === 3) {
        $code .= '-';
      }
    }

    return $code;
  }

}
