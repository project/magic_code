# Magic Code Verify Form

This submodule provides the base verification form that other modules can extend
when implementing a magic code based verification.

See the [`magic_code_email_login`]('../magic_code_email_login/README.md') module
for an example.
