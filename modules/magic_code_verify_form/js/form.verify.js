(function (Drupal) {
  Drupal.behaviors.magicCodeVerifyInput = {
    attach(context) {
      once("magicCodeVerifyInput", ".verify-code-input", context).forEach(
        /**
         * @param {HTMLElement} element
         */
        (element) => {
          const inputs = element.querySelectorAll("input");

          /**
           * Map the code to each individual input.
           */
          function setCode(code) {
            code
              .replace("-", "")
              .split("")
              .forEach((char, index) => {
                inputs[index].value = char;
              });
          }

          inputs.forEach((input, index) => {
            // Handle input.
            input.addEventListener("input", () => {
              if (input.value === "") {
                return;
              }

              // If input, focus next one.
              if (index < 5) {
                inputs[index + 1]?.focus();
              }
            });

            // Handle keydown (backspace).
            input.addEventListener("keydown", (event) => {
              if (input.value !== "" || event.key !== "Backspace") {
                return;
              }

              // Focus previous one.
              if (index > 0) {
                inputs[index - 1]?.focus();
              }
            });

            // Handle focus.
            input.addEventListener("focus", () => {
              if (input.value === "") {
                return;
              }

              input.select();
            });

            // Handle paste.
            input.addEventListener("paste", (event) => {
              event.preventDefault();

              const text = event.clipboardData.getData("text").trim();

              // Validate input.
              if (text.length !== 7 || text[3] !== "-") {
                return;
              }

              setCode(text);
            });
          });

          /**
           * Prefill from query params.
           */
          const code = new URLSearchParams(location.search).get('code');
          if (code) {
            setCode(code);
          }
        },
      );
    },
  };
})(Drupal);
