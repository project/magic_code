<?php

/**
 * @file
 * Hooks provided by magic_code module.
 */

declare(strict_types=1);

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Add custom magic-code operations to tokens.
 *
 * This example would add the token [user:magic-code-my-custom-operation].
 */
function hook_magic_code_user_mail_token_operations(&$operations) {
  $operations[] = 'my-custom-operation';
}

/**
 * Alters if given message should have token replaced by magic-code module.
 */
function hook_magic_code_replace_token_in_email(&$replaceInEmail, $context) {
  $message = $context['message'];

  if ($message['id'] === 'my-message-id') {
    $replaceInEmail = TRUE;
  }
}

/**
 * @} End of "addtogroup hooks"
 */
