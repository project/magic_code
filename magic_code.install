<?php

/**
 * @file
 * Install file.
 */

declare(strict_types=1);

/**
 * Update Drupal 10, Module 2.x, No 1.
 *
 * Update existing config to new flood config schema.
 */
function magic_code_update_10201() {
  $floodConfig = \Drupal::configFactory()->get('magic_code.settings')->get('flood');
  // Skip, if no floodConfig is set.
  if (!$floodConfig) {
    return;
  }

  // Add new flood config for creation and verification,
  // migrate existing config that was previously just used
  // for verification and delete the obsolete key.
  \Drupal::configFactory()->getEditable('magic_code.settings')
    ->set('flood_creation', [
      'ip_limit' => 50,
      'ip_window' => 3600,
      'user_limit' => 10,
      'user_window' => 900,
    ])
    ->set('flood_verification', $floodConfig)
    ->clear('flood')
    ->save();
}
