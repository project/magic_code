<?php

declare(strict_types=1);

namespace Drupal\magic_code;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\consumers\Negotiator;
use Drupal\magic_code\Entity\MagicCodeInterface;
use Drupal\magic_code\Exception\DuplicateMagicCodeException;
use Drupal\magic_code\Exception\MagicCodeCreationException;
use Drupal\magic_code\Flood\MagicCodeFloodControlInterface;
use Drupal\magic_code\Flood\MagicCodeFloodOperation;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Manager service for Magic Code entities.
 */
class MagicCodeManager implements MagicCodeManagerInterface {

  const MAX_RANDOM_CODE_GENERATION_ATTEMPTS = 10;

  /**
   * Construct a new MagicCodeManager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected TimeInterface $time,
    protected MagicCodeFloodControlInterface $flood,
    #[Autowire('@consumer.negotiator')]
    protected Negotiator $consumerNegotiator,
    #[Autowire('@logger.channel.magic_code')]
    protected LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function createNew(string $operation, AccountInterface $user, ?ConsumerInterface $consumer = NULL, ?string $email = NULL): MagicCodeInterface {
    $loginPermittedOperations = $this->getLoginPermittedOperations();
    $allowLogin = in_array($operation, $loginPermittedOperations);

    $expire = $this->time->getRequestTime() + $this->getCodeTtl();
    $code = $this->createUniqueCode();
    $targetEmail = $email ?? $user->getEmail();

    // Check if IP is blocked for magic code creation.
    if (!$this->flood->isIpAllowed(MagicCodeFloodOperation::Creation)) {
      // Always register a IP event.
      $this->flood->registerIp(MagicCodeFloodOperation::Creation);

      $this->logger->warning('Magic code creation failed for operation @operation and user @user. IP blocked.', [
        '@operation' => $operation,
        '@user' => $user->id(),
      ]);

      throw new MagicCodeCreationException('ip_blocked');
    }

    // Check if user is blocked for magic code creation.
    if (!$this->flood->isUserAllowed(MagicCodeFloodOperation::Creation, $user->id())) {
      // Always register a IP event.
      $this->flood->registerIp(MagicCodeFloodOperation::Creation);

      $this->logger->warning('Magic code creation failed for operation @operation and user @user. User blocked.', [
        '@operation' => $operation,
        '@user' => $user->id(),
      ]);

      throw new MagicCodeCreationException('user_blocked');
    }

    // Always register a IP and user event.
    $this->flood->registerIp(MagicCodeFloodOperation::Creation);
    $this->flood->registerUser(MagicCodeFloodOperation::Creation, $user->id());

    // Load consumer via negotiator if not set.
    if (!$consumer) {
      $consumer = $this->consumerNegotiator->negotiateFromRequest();
    }

    $payload = [
      'auth_user_id' => $user->id(),
      'value' => $code,
      'operation' => $operation,
      'expire' => $expire,
      'email' => $targetEmail,
      'status' => TRUE,
      'login_allowed' => $allowLogin,
      'client' => [
        'target_id' => $consumer->id(),
      ],
    ];

    $magicCode = $this->entityTypeManager->getStorage('magic_code')->create($payload);
    $magicCode->save();

    return $magicCode;
  }

  /**
   * {@inheritdoc}
   */
  public function verify(
    string $codeValue,
    string $operation,
    int $mode,
    AccountInterface $user,
    ?ConsumerInterface $consumer = NULL,
    ?string $email = NULL,
  ): MagicCodeResult {
    $targetEmail = $email ?? $user->getEmail();

    // Flood protection: This is needed to prevent brute force attacks via
    // magic codes. The implementation is very similar to the basic_auth module.
    // @see \Drupal\basic_auth\Authentication\Provider\BasicAuth::authenticate()
    // Do not allow magic code verification from the current user's IP if
    // the limit has been reached. Default is 50 failed verifications in one
    // hour. This is independent of the per-user limit to catch attempts from
    // one IP to verify many different magic codes. We have a reasonably high
    // limit since there may be only one apparent IP for all users at an
    // institution.
    // Important: Always check IP limit before the user limit and never register
    // a failed user attempt if the IP is blocked! That would otherwise enable
    // an attacker to do a DOS (but not DDOS) attack for the given user,
    // even if the attacker's IP is already blocked.
    //
    // First, we check the IP limit.
    // If this limit is reached, we abort early and register a
    // IP failed attempt.
    if (!$this->flood->isIpAllowed(MagicCodeFloodOperation::Verification)) {
      // Always register IP verification failure.
      $this->flood->registerIp(MagicCodeFloodOperation::Verification);

      $this->logger->warning('Magic code verification failed for user @user. IP blocked.', ['@user' => $user->id()]);

      return MagicCodeResult::BlockedByIp;
    }

    // Now, we check the user limit.
    // If this limit is reached, we register an IP and User failed attempt.
    if (!$this->flood->isUserAllowed(MagicCodeFloodOperation::Verification, $user->id())) {
      // Always register IP verification failure.
      $this->flood->registerIp(MagicCodeFloodOperation::Verification);

      $this->logger->warning('Magic code verification failed for user @user. User blocked.', ['@user' => $user->id()]);

      return MagicCodeResult::BlockedByUser;
    }

    // Load consumer via negotiator if not set.
    if (!$consumer) {
      $consumer = $this->consumerNegotiator->negotiateFromRequest();
    }

    // User and IP are not blocked, continue with verification.
    $query = $this->entityTypeManager->getStorage('magic_code')->getQuery();

    $query = $query
      ->accessCheck(FALSE)
      ->condition('value', $codeValue)
      ->condition('auth_user_id', $user->id())
      ->condition('email', $targetEmail)
      ->condition('operation', $operation)
      ->condition('client', $consumer->id())
      ->condition('status', TRUE)
      ->condition('expire', $this->time->getRequestTime(), '>=');

    // If verifying login, the login must be allowed.
    if ($mode === self::VERIFY_MODE_LOGIN) {
      $query->condition('login_allowed', TRUE);
    }

    $ids = $query
      ->execute();

    // No match found.
    if (empty($ids)) {
      // Register verification failure.
      $this->flood->registerIp(MagicCodeFloodOperation::Verification);
      $this->flood->registerUser(MagicCodeFloodOperation::Verification, $user->id());

      $this->logger->warning('Magic code @code is not found for user @user.', [
        '@code' => $codeValue,
        '@user' => $user->id(),
      ]);

      return MagicCodeResult::Invalid;
    }

    $id = reset($ids);

    // Load code entity.
    $entity = $this->entityTypeManager->getStorage('magic_code')->load($id);
    /** @var \Drupal\magic_code\Entity\MagicCodeInterface $entity */

    // Revoke login if mode is login.
    if ($mode === self::VERIFY_MODE_LOGIN) {
      $entity->revokeLogin()->save();
    }

    // Revoke code itself for all other modes, except
    // if the operation is login.
    if ($mode !== self::VERIFY_MODE_LOGIN || $operation === 'login') {
      $entity->revoke()->save();
    }

    // Clear user creation and verification limit.
    $this->flood->clearUser(MagicCodeFloodOperation::Creation, $user->id());
    $this->flood->clearUser(MagicCodeFloodOperation::Verification, $user->id());

    return MagicCodeResult::Success;
  }

  /**
   * {@inheritdoc}
   */
  public function revoke(string $id) {
    /** @var \Drupal\magic_code\Entity\MagicCodeInterface $code */
    $code = $this->entityTypeManager->getStorage('magic_code')->load($id);

    if ($code) {
      $code->revoke()->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function revokeMultiple(array $ids) {
    foreach ($ids as $id) {
      $this->revoke($id);
    }
  }

  /**
   * Tries to generate a unique magic code value.
   *
   * Gives up after MAX_RANDOM_CODE_GENERATION_ATTEMPTS attempts.
   *
   * @return string
   *   The generated magic code.
   */
  protected function createUniqueCode(): string {
    $maxGenerationAttempts = self::MAX_RANDOM_CODE_GENERATION_ATTEMPTS;
    while ($maxGenerationAttempts-- > 0) {
      $code = $this->generateCode();

      // Query for existing entity with same code.
      $existingEntityCount = $this->entityTypeManager
        ->getStorage('magic_code')
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('value', $code)
        ->count()
        ->execute();

      // Code is unique.
      if ($existingEntityCount === 0) {
        return $code;
      }
    }

    throw new DuplicateMagicCodeException('Magic code value must be unique.');
  }

  /**
   * Generates the magic code value.
   *
   * This results in a six digit code with
   * a dash in the middle.
   *
   * E.g.: 2CV-UGB.
   *
   * @return string
   *   The generated magic code.
   */
  protected function generateCode(): string {
    // Pool of alphanumeric chars, excluding 0 and O to
    // avoid user confusion.
    $chars = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';

    $buf = '';

    // Generate code.
    for ($i = 0; $i < 6; ++$i) {
      $buf .= $chars[random_int(0, strlen($chars) - 1)];

      // Add dash.
      if ($i === 2) {
        $buf .= '-';
      }
    }

    return $buf;
  }

  /**
   * Get the magic code ttl from the config.
   *
   * @return int
   *   The magic code ttl.
   */
  protected function getCodeTtl() {
    return (int) $this->configFactory->get('magic_code.settings')->get('code_ttl');
  }

  /**
   * Gets login permitted operations from config.
   *
   * @return array
   *   An array of operations that permit login.
   */
  protected function getLoginPermittedOperations() {
    return $this->configFactory->get('magic_code.settings')->get('login_permitted_operations');
  }

}
