<?php

declare(strict_types=1);

namespace Drupal\magic_code\Exception;

/**
 * Exception for when magic code creation fails.
 */
class MagicCodeCreationException extends \Exception {

  /**
   * Construct new MagicCodeCreationException.
   *
   * @param 'unknown'|'ip_blocked'|'user_blocked' $reason
   *   The reason for creation failure.
   * @param int $code
   *   [optional] The Exception code.
   * @param null|\Throwable $previous
   *   [optional] The previous throwable used for the exception chaining.
   */
  public function __construct(string $reason, int $code = 0, \Throwable|null $previous = NULL) {
    $specificMessage = match($reason) {
      'unknown' => 'unknown reason',
      'ip_blocked' => 'IP is temporarily blocked for creating new magic codes',
      'user_blocked' => 'user is temporarily blocked for creating new magic codes',
    };

    $message = 'Could not create magic code: ' . $specificMessage;

    parent::__construct($message, $code, $previous);
  }

}
