<?php

declare(strict_types=1);

namespace Drupal\magic_code\VerificationProvider;

use Drupal\Core\Session\AccountInterface;
use Drupal\consumers\Entity\ConsumerInterface;
use Drupal\consumers\Negotiator;
use Drupal\magic_code\MagicCodeManagerInterface;
use Drupal\magic_code\MagicCodeResult;
use Drupal\verification\Provider\VerificationProviderInterface;
use Drupal\verification\Result\VerificationResult;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Request;

/**
 * Magic code verification provider plugin.
 */
class MagicCodeVerificationProvider implements VerificationProviderInterface {

  const HEADER_MAGIC_CODE = 'X-Verification-Magic-Code';

  const UNHANDLED_NO_CONSUMER = 'magic_code_no_consumer_found';
  const ERR_INVALID_CODE = 'magic_code_invalid_code';

  /**
   * Construct new Magic Code provider plugin.
   */
  public function __construct(
    #[Autowire('@magic_code.manager')]
    protected MagicCodeManagerInterface $magicCodeManager,
    #[Autowire('@consumer.negotiator')]
    protected Negotiator $negotiator,
    #[Autowire('@logger.channel.magic_code')]
    protected LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function verifyOperation(Request $request, string $operation, AccountInterface $user, ?string $email = NULL): VerificationResult {
    $callback = $this->coreVerify($request);
    if ($callback instanceof VerificationResult) {
      return $callback;
    }

    return $callback(
      function (ConsumerInterface $consumer, string $code) use ($operation, $user, $email) {
        $result = $this->magicCodeManager->verify(
          $code,
          $operation,
          MagicCodeManagerInterface::VERIFY_MODE_OPERATION,
          $user,
          $consumer,
          $email,
        );

        if ($result === MagicCodeResult::Success) {
          return VerificationResult::ok();
        }

        return VerificationResult::err(
          'magic_code_' . $result->value,
        );
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function verifyLogin(Request $request, string $operation, AccountInterface $user, ?string $email = NULL): VerificationResult {
    $callback = $this->coreVerify($request);
    if ($callback instanceof VerificationResult) {
      return $callback;
    }

    return $callback(
      function (ConsumerInterface $consumer, string $code) use ($operation, $user, $email) {
        $result = $this->magicCodeManager->verify(
          $code,
          $operation,
          MagicCodeManagerInterface::VERIFY_MODE_LOGIN,
          $user,
          $consumer,
          $email,
        );

        if ($result === MagicCodeResult::Success) {
          return VerificationResult::ok();
        }

        return VerificationResult::err(
          'magic_code_' . $result->value,
        );
      }
    );
  }

  /**
   * Common verification logic.
   *
   * This method implements the common verification logic
   * used by all verification methods.
   *
   * Specific verification is passed as a closure to the
   * return function of this method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Closure|VerificationResult
   *   A closure that executes specific verification logic
   *   or a VerificationResult.
   */
  protected function coreVerify(Request $request) {
    $consumer = $this->negotiator->negotiateFromRequest($request);
    if (!$consumer) {
      $this->logger->error('Consumer could not be negotiated for request!');

      return VerificationResult::unhandled(self::UNHANDLED_NO_CONSUMER);
    }

    // No verification data found.
    if (!$request->headers->has(self::HEADER_MAGIC_CODE)) {
      return VerificationResult::unhandled();
    }

    $code = $request->headers->get(self::HEADER_MAGIC_CODE);
    if (!$code) {
      return VerificationResult::err(self::ERR_INVALID_CODE);
    }

    return function (\Closure $innerVerify) use ($consumer, $code) {
      return $innerVerify($consumer, $code);
    };
  }

}
