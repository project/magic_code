<?php

declare(strict_types=1);

namespace Drupal\magic_code\Entity\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting Magic Code entities.
 */
class MagicCodeDeleteForm extends ContentEntityConfirmFormBase {

  use AutowireTrait;

  /**
   * Construct a new MagicCodeDeleteForm.
   */
  public function __construct(
    EntityRepositoryInterface $entityRepository,
    MessengerInterface $messenger,
    ?EntityTypeBundleInfoInterface $entityTypeBundleInfo = NULL,
    ?TimeInterface $time = NULL,
  ) {
    parent::__construct($entityRepository, $entityTypeBundleInfo, $time);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    $this->entity->delete();

    $this->messenger->addMessage(
      $this->t('content @type: deleted @label.',
        [
          '@type' => $this->entity->bundle(),
          '@label' => $this->entity->label(),
        ]
        )
    );

    $formState->setRedirectUrl(new Url('entity.magic_code.collection'));
  }

}
