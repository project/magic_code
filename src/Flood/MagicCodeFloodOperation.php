<?php

declare(strict_types=1);

namespace Drupal\magic_code\Flood;

/**
 * Defines a magic code flood operation.
 *
 * This is to indicate which operation should be checked.
 */
enum MagicCodeFloodOperation {

  case Creation;
  case Verification;

}
