<?php

declare(strict_types=1);

namespace Drupal\magic_code\Flood;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Flood\FloodInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Magic Code Flood Control service.
 *
 * @see: \Drupal\Core\Flood\DatabaseBackend
 */
class MagicCodeFloodControl implements MagicCodeFloodControlInterface {

  public const ID_IP_CREATION = 'magic_code.creation_ip';
  public const ID_USER_CREATION = 'magic_code.creation_user';
  public const ID_IP_VERIFICATION = 'magic_code.failed_verification_ip';
  public const ID_USER_VERIFICATION = 'magic_code.failed_verification_user';

  public function __construct(
    protected FloodInterface $flood,
    protected RequestStack $requestStack,
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function isIpAllowed(MagicCodeFloodOperation $operation): bool {
    $floodConfig = $this->getFloodConfig();

    return match($operation) {
      MagicCodeFloodOperation::Creation => $this->flood->isAllowed(
        self::ID_IP_CREATION,
        $floodConfig['creation']['ip_limit'],
        $floodConfig['creation']['ip_window'],
      ),
      MagicCodeFloodOperation::Verification => $this->flood->isAllowed(
        self::ID_IP_VERIFICATION,
        $floodConfig['verification']['ip_limit'],
        $floodConfig['verification']['ip_window'],
      ),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function isUserAllowed(MagicCodeFloodOperation $operation, int|string $uid): bool {
    $floodConfig = $this->getFloodConfig();

    return match($operation) {
      MagicCodeFloodOperation::Creation => $this->flood->isAllowed(
        self::ID_USER_CREATION,
        $floodConfig['creation']['user_limit'],
        $floodConfig['creation']['user_window'],
        $this->getUserFloodIdentifier($uid),
      ),
      MagicCodeFloodOperation::Verification => $this->flood->isAllowed(
        self::ID_USER_VERIFICATION,
        $floodConfig['verification']['user_limit'],
        $floodConfig['verification']['user_window'],
        $this->getUserFloodIdentifier($uid),
      ),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function registerIp(MagicCodeFloodOperation $operation): void {
    $floodConfig = $this->getFloodConfig();

    switch ($operation) {
      case MagicCodeFloodOperation::Creation:
        $this->flood->register(
          self::ID_IP_CREATION,
          $floodConfig['creation']['ip_window'],
        );
        break;

      case MagicCodeFloodOperation::Verification:
        $this->flood->register(
          self::ID_IP_VERIFICATION,
          $floodConfig['verification']['ip_window'],
        );
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function registerUser(MagicCodeFloodOperation $operation, int|string $uid): void {
    $floodConfig = $this->getFloodConfig();

    switch ($operation) {
      case MagicCodeFloodOperation::Creation:
        $this->flood->register(
          self::ID_USER_CREATION,
          $floodConfig['creation']['user_window'],
          $this->getUserFloodIdentifier($uid),
        );
        break;

      case MagicCodeFloodOperation::Verification:
        $this->flood->register(
          self::ID_USER_VERIFICATION,
          $floodConfig['verification']['user_window'],
          $this->getUserFloodIdentifier($uid),
        );
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearIp(MagicCodeFloodOperation $operation): void {
    switch ($operation) {
      case MagicCodeFloodOperation::Creation:
        $this->flood->clear(self::ID_IP_CREATION);
        break;

      case MagicCodeFloodOperation::Verification:
        $this->flood->clear(self::ID_IP_VERIFICATION);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function clearUser(MagicCodeFloodOperation $operation, int|string $uid): void {
    switch ($operation) {
      case MagicCodeFloodOperation::Creation:
        $this->flood->clear(self::ID_USER_CREATION, $this->getUserFloodIdentifier($uid));
        break;

      case MagicCodeFloodOperation::Verification:
        $this->flood->clear(self::ID_USER_VERIFICATION, $this->getUserFloodIdentifier($uid));
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isAllowed($name, $threshold, $window = 3600, $identifier = NULL) {
    if ($this->flood->isAllowed($name, $threshold, $window, $identifier)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function register($name, $window = 3600, $identifier = NULL) {
    return $this->flood->register($name, $window, $identifier);
  }

  /**
   * {@inheritdoc}
   */
  public function clear($name, $identifier = NULL) {
    return $this->flood->clear($name, $identifier);
  }

  /**
   * {@inheritdoc}
   */
  public function garbageCollection() {
    return $this->flood->garbageCollection();
  }

  /**
   * Get the magic code flood config.
   *
   * @return array{creation: array{'ip_limit': int, 'ip_window': int, 'user_limit': int, 'user_window': int}, verification: array{'ip_limit': int, 'ip_window': int, 'user_limit': int, 'user_window': int}}
   *   The magic code flood config.
   */
  protected function getFloodConfig() {
    return [
      'creation' => $this->configFactory->get('magic_code.settings')->get('flood_creation'),
      'verification' => $this->configFactory->get('magic_code.settings')->get('flood_verification'),
    ];
  }

  /**
   * Get the user flood identifier.
   *
   * Depends on the global `uid_only` setting in `user.flood`.
   */
  public function getUserFloodIdentifier(int|string $uid): ?string {
    if (!$uid) {
      throw new \Exception('User id must be set for magic code flood mode user.');
    }

    $floodConfig = $this->configFactory->get('user.flood');

    if ($floodConfig->get('uid_only')) {
      // Register flood events based on the uid only, so they apply for any
      // IP address. This is the most secure option.
      $identifier = $uid;
    }
    else {
      // The default identifier is a combination of uid and IP address. This
      // is less secure but more resistant to denial-of-service attacks that
      // could lock out all users with public user names.
      $identifier = $uid . '-' . $this->requestStack->getCurrentRequest()->getClientIP();
    }

    return $identifier;
  }

}
