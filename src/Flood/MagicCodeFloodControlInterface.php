<?php

declare(strict_types=1);

namespace Drupal\magic_code\Flood;

use Drupal\Core\Flood\FloodInterface;

/**
 * Defines an interface for magic code flood controllers.
 */
interface MagicCodeFloodControlInterface extends FloodInterface {

  /**
   * Checks whether ip address is not blocked for given operation.
   */
  public function isIpAllowed(MagicCodeFloodOperation $operation): bool;

  /**
   * Checks whether user is not blocked for given operation.
   */
  public function isUserAllowed(MagicCodeFloodOperation $operation, string|int $uid): bool;

  /**
   * Register a ip based flood event.
   */
  public function registerIp(MagicCodeFloodOperation $operation): void;

  /**
   * Register a user based flood event.
   */
  public function registerUser(MagicCodeFloodOperation $operation, string|int $uid): void;

  /**
   * Clear a ip based flood event.
   */
  public function clearIp(MagicCodeFloodOperation $operation): void;

  /**
   * Clear a user based flood event.
   */
  public function clearUser(MagicCodeFloodOperation $operation, string|int $uid): void;

}
