<?php

declare(strict_types=1);

namespace Drupal\Tests\magic_code\Functional;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\consumers\Entity\Consumer;
use Drupal\magic_code\MagicCodeManagerInterface;
use Drush\TestTraits\DrushTestTrait;

/**
 * Test MagicCode Drush Commands.
 */
class MagicCodeCommandsTest extends BrowserTestBase {

  use DrushTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'image',
    'file',
    'magic_code',
  ];

  /**
   * The client.
   */
  protected Consumer $client;

  /**
   * The manager service.
   */
  protected MagicCodeManagerInterface $manager;

  /**
   * The magic code entity storage.
   */
  protected EntityStorageInterface $magicCodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Delete default consumer.
    Consumer::load('1')->delete();

    $this->client = Consumer::create([
      'client_id' => 'test',
      'label' => 'test',
      'is_default' => TRUE,
    ]);
    $this->client->save();

    $this->manager = $this->container->get('magic_code.manager');
    $this->magicCodeStorage = $this->container->get('entity_type.manager')->getStorage('magic_code');
  }

  /**
   * Test magic code generate command.
   */
  public function testGenerateMagicCode() {
    $operation = 'demo-operation';
    $user = $this->drupalCreateUser(
      values: [
        'mail' => 'user@example.com',
      ]
    );

    //
    // Error path.
    //
    // Missing operation.
    $this->drush(
      command: 'magic-code:generate',
      expected_return: 1,
    );
    $this->assertStringContainsString('missing: "operation"', $this->getErrorOutput());

    // Missing user id.
    $this->drush(
      command: 'magic-code:generate',
      args: [$operation],
    );
    $this->assertStringContainsString('No user ID specified', $this->getOutput());

    // Wrong user id.
    $this->drush(
      command: 'magic-code:generate',
      args: [$operation],
      options: [
        'uid' => '99',
      ],
    );
    $this->assertStringContainsString('No user found', $this->getOutput());

    // Wrong client-id.
    $this->drush(
      command: 'magic-code:generate',
      args: [$operation],
      options: [
        'uid' => $user->id(),
        'client-id' => '99',
      ],
    );
    $this->assertStringContainsString('No client found', $this->getOutput());

    //
    // Success path.
    //
    // With default consumer.
    $this->drush(
      command: 'magic-code:generate',
      args: [$operation],
      options: [
        'uid' => $user->id(),
      ],
    );

    $output = $this->getOutput();
    $code = $this->extractMagicCode($output);
    $result = $this->magicCodeStorage->loadByProperties(['value' => $code]);

    /** @var \Drupal\magic_code\Entity\MagicCodeInterface $codeEntity */
    $codeEntity = reset($result);

    $this->assertNotEquals(FALSE, $codeEntity);
    $this->assertEquals($this->client->id(), $codeEntity->get('client')->entity->id());

    // With dedicated consumer.
    $consumer = Consumer::create([
      'client_id' => 'new',
      'label' => 'new',
    ]);
    $consumer->save();

    $this->drush(
      command: 'magic-code:generate',
      args: [$operation],
      options: [
        'uid' => $user->id(),
        'client-id' => 'new',
      ],
    );

    $output = $this->getOutput();
    $code = $this->extractMagicCode($output);
    $result = $this->magicCodeStorage->loadByProperties(['value' => $code]);

    /** @var \Drupal\magic_code\Entity\MagicCodeInterface $codeEntity */
    $codeEntity = reset($result);

    $this->assertNotNull($codeEntity);
    $this->assertEquals($consumer->id(), $codeEntity->get('client')->entity->id());
  }

  /**
   * Extract magic code from drush command output.
   */
  protected function extractMagicCode(string $output): string {
    if (preg_match('/[A-Z0-9]{3}-[A-Z0-9]{3}/', $output, $matches)) {
      return $matches[0];
    }
    return $output;
  }

}
