<?php

declare(strict_types=1);

namespace Drupal\Tests\magic_code\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Test the magic code alteration of the flood control form.
 */
class MagicCodeFloodControlFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'magic_code',
    'flood_control',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test the form.
   */
  public function testForm() {
    // Login as admin.
    $admin = $this->drupalCreateUser(admin: TRUE);
    $this->drupalLogin($admin);

    // Goto delete form for this code.
    $this->drupalGet('/admin/config/people/flood-control');

    // Make sure default values are set on form.
    $this->assertSession()->fieldValueEquals('magic_code_creation_ip_limit', 50);
    $this->assertSession()->fieldValueEquals('magic_code_creation_ip_window', 3600);
    $this->assertSession()->fieldValueEquals('magic_code_creation_user_limit', 10);
    $this->assertSession()->fieldValueEquals('magic_code_creation_user_window', 900);
    $this->assertSession()->fieldValueEquals('magic_code_verification_ip_limit', 50);
    $this->assertSession()->fieldValueEquals('magic_code_verification_ip_window', 3600);
    $this->assertSession()->fieldValueEquals('magic_code_verification_user_limit', 5);
    $this->assertSession()->fieldValueEquals('magic_code_verification_user_window', 21600);
  }

  /**
   * Test form save.
   */
  public function testFormSave() {
    // Login as admin.
    $admin = $this->drupalCreateUser(admin: TRUE);
    $this->drupalLogin($admin);

    // Goto delete form for this code.
    $this->drupalGet('/admin/config/people/flood-control');

    $page = $this->getSession()->getPage();

    $page->fillField('magic_code_creation_ip_limit', 5);
    $page->fillField('magic_code_creation_ip_window', 60);
    $page->fillField('magic_code_creation_user_limit', 20);
    $page->fillField('magic_code_creation_user_window', 180);
    $page->fillField('magic_code_verification_ip_limit', 125);
    $page->fillField('magic_code_verification_ip_window', 300);
    $page->fillField('magic_code_verification_user_limit', 50);
    $page->fillField('magic_code_verification_user_window', 600);
    $page->pressButton('Save configuration');

    $creationConfig = $this->config('magic_code.settings')->get('flood_creation');
    $verificationConfig = $this->config('magic_code.settings')->get('flood_verification');

    $this->assertEquals(5, $creationConfig['ip_limit']);
    $this->assertSession()->fieldValueEquals('magic_code_creation_ip_limit', 5);
    $this->assertEquals(60, $creationConfig['ip_window']);
    $this->assertSession()->fieldValueEquals('magic_code_creation_ip_window', 60);
    $this->assertEquals(20, $creationConfig['user_limit']);
    $this->assertSession()->fieldValueEquals('magic_code_creation_user_limit', 20);
    $this->assertEquals(180, $creationConfig['user_window']);
    $this->assertSession()->fieldValueEquals('magic_code_creation_user_window', 180);

    $this->assertEquals(125, $verificationConfig['ip_limit']);
    $this->assertSession()->fieldValueEquals('magic_code_verification_ip_limit', 125);
    $this->assertEquals(300, $verificationConfig['ip_window']);
    $this->assertSession()->fieldValueEquals('magic_code_verification_ip_window', 300);
    $this->assertEquals(50, $verificationConfig['user_limit']);
    $this->assertSession()->fieldValueEquals('magic_code_verification_user_limit', 50);
    $this->assertEquals(600, $verificationConfig['user_window']);
    $this->assertSession()->fieldValueEquals('magic_code_verification_user_window', 600);
  }

}
