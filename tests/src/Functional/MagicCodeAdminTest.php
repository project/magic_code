<?php

declare(strict_types=1);

namespace Drupal\Tests\magic_code\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\magic_code\MagicCodeManagerInterface;

/**
 * Test the magic code admin forms.
 */
class MagicCodeAdminTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'magic_code',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Magic code manager.
   */
  protected MagicCodeManagerInterface $magicCodeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->magicCodeManager = $this->container->get(MagicCodeManagerInterface::class);
  }

  /**
   * Test the delete form.
   */
  public function testDeleteForm() {
    // Login as admin.
    $admin = $this->drupalCreateUser(admin: TRUE);
    $this->drupalLogin($admin);

    // Create code.
    $code = $this->magicCodeManager->createNew('login', $admin);

    // Goto delete form for this code.
    $this->drupalGet('/admin/config/people/magic-code/' . $code->id() . '/delete');

    // Delete it.
    $this->submitForm([], 'Delete');

    // Confirm deletion.
    $this->assertSession()->pageTextContains('There are no magic code entities yet.');
  }

}
